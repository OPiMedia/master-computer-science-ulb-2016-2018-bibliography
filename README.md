# *Master Computer Science ULB 2016-2018 Bibliography*

List of references for some ULB master's courses.

  - [PDF document](https://bitbucket.org/OPiMedia/master-computer-science-ulb-2016-2018-bibliography/raw/master/Master-Computer-Science--ULB-2016-2018--Bibliography.pdf)
  - [BibLaTeX file](https://bitbucket.org/OPiMedia/master-computer-science-ulb-2016-2018-bibliography/raw/master/biblio_src/bib/CS_ULB_biblio.bib)
  - [Zotero file](https://bitbucket.org/OPiMedia/master-computer-science-ulb-2016-2018-bibliography/raw/master/biblio_src/bib/Zotero/CS_ULB_biblio.rdf)

Some course notes and personal works:
[Master in Computer science, 1st year (ULB 2016 – 2017)](http://www.opimedia.be/CV/2016-2017-ULB/)

All documents and LaTeX sources are available on this Bitbucket repository:
<https://bitbucket.org/OPiMedia/master-computer-science-ulb-2016-2018-bibliography>



## Author: 🌳 Olivier Pirson — OPi ![OPi][OPi] 🇧🇪🇫🇷🇬🇧 🐧 👨‍💻 👨‍🔬
🌐 Website: <http://www.opimedia.be/>

💾 Bitbucket: <https://bitbucket.org/OPiMedia/>

  - 📧 <olivier.pirson.opi@gmail.com>
  - Mastodon: <https://mamot.fr/@OPiMedia> — Twitter: <https://twitter.com/OPirson>
  - other profiles: <http://www.opimedia.be/about/>

[OPi]: http://www.opimedia.be/_png/OPi.png



![Master Computer Science ULB 2016-2018 Bibliography](https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2019/Nov/13/3660036908-10-master-computer-science-ulb-2016-201_avatar.png)
